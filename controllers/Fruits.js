const { loadFruits } = require('../models/fruitsModel');

const getAllFruits = (req, res) => {
    res.send(loadFruits());
};

module.exports = getAllFruits;