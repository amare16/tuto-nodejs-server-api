const { readFileSync } = require('fs');
const path = require('path');
const jsonFilePath = path.join(__dirname, '../dataFromJson/allMicroNutrients.json')
let loadMicroNutrients = () => JSON.parse(readFileSync(jsonFilePath));

module.exports = { loadMicroNutrients };