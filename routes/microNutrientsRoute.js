const express = require('express');
const router = express.Router();

const getAllMicroNutrients = require('../controllers/MicroNutrients');

router.get('/micro-nutrients', getAllMicroNutrients);

module.exports = router;